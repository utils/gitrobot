#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import re
from Utility import *

class CheckWhitespace:
    def __init__(self, approx_tabs = True):
        self._approx_tabs = approx_tabs
        self._parse_attr = re.compile(r'^(?P<path>[^:]*): (?P<attr>[^:]*): (?P<info>.*)$')
        self._match_attr = re.compile(r'(^|,)tab-in-indent(,|$)')
        self._match_tabs = re.compile('^\\+\t', flags = re.MULTILINE)

    def _check_whitespace(self, commit):
        # Validate whitespace
        res, out, err = git_safe('diff-tree', '--no-commit-id', '--root', '-c', '--check', commit.sha1)
        if res != 0:
            return ['commit %s adds bad whitespace:\n%s' % (commit.sha1[0:8], out)]
        else:
            return []

    def _check_tabs(self, commit):
        # Git 1.7.2 introduced tab-in-indent.  Approximate it for
        # older Git by rejecting leading TABs.
        failures = []
        for diff in commit.diffs():
            f = diff['name']
            attr = self._parse_attr.match(git('check-attr', 'whitespace', '--', f))
            if attr and self._match_attr.search(attr.group('info')):
                patch = git('diff-tree', '--no-commit-id', '--root', '-p',
                            '--pickaxe-regex', '-S^\t', commit.sha1, '--', f)
                if self._match_tabs.search(patch):
                    failures.append('commit %s adds leading TABs:\n%s' % (commit.sha1[0:8], patch))
        return failures

    def enforce(self, commit, user):
        failures = self._check_whitespace(commit)
        if self._approx_tabs:
            failures.extend(self._check_tabs(commit))
        if failures:
            fail('\n'.join(failures))
