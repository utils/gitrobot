#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import re
from Utility import *

class Commit:
    def __init__(self, sha1):
        raw = git('cat-file', 'commit', sha1)
        meta, log = raw.split('\n\n', 1)
        self.sha1 = sha1
        self.parents = []
        self.log = log
        self._parse_diffs_done = False
        for line in meta.splitlines():
            self._parse_meta(line)

    def _parse_meta(self, line):
        key, value = line.split(' ', 1)
        if key == 'parent':
            self.parents.append(value)
        else:
            self.__dict__[key] = value

    def _parse_diffs(self):
        self._parse_diffs_done = True
        self._diffs = []
        diffline = re.compile(r':' +
                              r'(?P<src_mode>[0-7]{6}) ' +
                              r'(?P<dst_mode>[0-7]{6}) ' +
                              r'(?P<src_obj>[0-9a-z]{40}) ' +
                              r'(?P<dst_obj>[0-9a-z]{40}) ' +
                              r'(?P<status>[ADMTUX])' + # [CR]
                              r'	(?P<name>.*)')
        mergeline = re.compile(r'::+' +
                               r'(?P<src_mode>[0-7]{6}( [0-7]{6})*) ' +
                               r'(?P<dst_mode>[0-7]{6}) ' +
                               r'(?P<src_obj>[0-9a-z]{40}( [0-9a-z]{40})*) ' +
                               r'(?P<dst_obj>[0-9a-z]{40}) ' +
                               r'(?P<status>[ADMTUX]+)' + # [CR]
                               r'	(?P<name>.*)')
        files = git('diff-tree', '--root', '-c', '-r', '--no-commit-id', self.sha1)
        for line in files.splitlines():
            diff = diffline.match(line)
            if diff:
                self._diffs.append(diff.groupdict())
                continue
            merge = mergeline.match(line)
            if merge:
                # Split merge diff up into individual diffs.
                src_modes = merge.group('src_mode').split(' ')
                src_objs = merge.group('src_obj').split(' ')
                dst_mode = merge.group('dst_mode')
                dst_obj = merge.group('dst_obj')
                stats = list(merge.group('status'))
                name = merge.group('name')
                if len(src_modes) != len(src_objs):
                    die('commit %s has unrecognized diff line:\n%s' % (self.sha1[0:8], line))
                if len(src_modes) != len(stats):
                    die('commit %s has unrecognized diff line:\n%s' % (self.sha1[0:8], line))
                for i in range(0, len(src_modes)):
                    d = {'src_mode': src_modes[i], 'dst_mode': dst_mode,
                         'src_obj': src_objs[i],   'dst_obj': dst_obj,
                         'status': stats[i],       'name': name}
                    self._diffs.append(d)
                continue
            die('commit %s has unrecognized diff line:\n%s' % (self.sha1[0:8], line))

    def diffs(self):
        if not self._parse_diffs_done:
            self._parse_diffs()
        return self._diffs
