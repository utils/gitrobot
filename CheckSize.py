#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import re
from Utility import *

class CheckSize:
    def __init__(self, maxsize = (1<<20)):
        self._maxsize = maxsize
        self._parse_attr = re.compile(r'^(?P<path>[^:]*): (?P<attr>[^:]*): (?P<info>.*)$')

    def enforce(self, commit, user):
        kb = 1.0/(1<<10)
        for diff in commit.diffs():
            f = diff['name']

            # Lookup custom max size attribute.
            attr = self._parse_attr.match(git('check-attr', 'hooks-max-size', '--', f))
            if attr: max = attr.group('info')
            else:    max = 'unspecified'

            # Check custom max size.
            maxsize = self._maxsize
            if max == 'unset': # No maximum.
                continue
            elif max != 'set' and max != 'unspecified': # Custom maximum.
                try: maxsize = int(max)
                except: fail('invalid value hooks-max-size="%s" for "%s"' % (max, f))

            # Check size of blob.
            dst_obj = diff['dst_obj']
            dst_mode = diff['dst_mode']
            if dst_obj != zero and dst_mode != '160000':
                fsize = int(git('cat-file', '-s', dst_obj))
                if fsize > maxsize:
                    fail('commit %s creates blob %s at path\n  %s\nwith size %d KB, greater than the maximum %d KB' %
                         (commit.sha1[0:8], dst_obj[0:8], f, fsize*kb, maxsize*kb))
