#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import os
import re

class User:
    def __init__(self, gerrit_uploader = None):
        if gerrit_uploader:
            self._init_gerrit(gerrit_uploader)
        else:
            self._init_gitolite()
        self._parse_email()

    def _init_gerrit(self, uploader):
        matcher = re.compile('^(?P<name>[^@(]+)\s+\((?P<email>[^@]+(@[^@]+)?)\)')
        match = matcher.match(uploader)
        if match:
            self.name = match.group('name')
            self.email = match.group('email')
        else:
            self.name = None
            self.email = None

    def _init_gitolite(self):
        # Find the user email address in the environment.
        if os.environ.has_key('GL_USER'):
            self.email = os.environ['GL_USER']
        elif os.environ.has_key('USER'):
            self.email = os.environ['USER']
        else:
            self.email = None

        # Save the real user in case of gl-setuid-command.
        if os.environ.has_key('GL_REAL_USER'):
            self.real = os.environ['GL_REAL_USER']
        else:
            self.real = None

        # Parse the real name out from the ssh key comment.
        self.name = None
        try:
            gldir = os.environ['GL_ADMINDIR']
            keyfile = os.path.join(gldir, 'keydir', self.email+'.pub')
            f = open(keyfile, 'r')
            key = f.readline()
            f.close()
            m = re.match(r'^.*ssh-(rsa|dss) [^ ]+ *(?P<name>[^<]*)<(?P<email>[^>]*)> *$', key)
            if m.group('email') == self.email:
                self.name = m.group('name').strip()
        except:
            pass

    def _parse_email(self):
        # Parse the email address.
        try:
            self.user, self.domain = self.email.rsplit('@', 1)
        except:
            self.user = self.email
            self.domain = ''

    def __nonzero__(self):
        return bool(self.email)

    def __str__(self):
        if self.name:
            return '%s <%s>' % (self.name, self.email)
        else:
            return '%s' % self.email
